module.exports = {
  theme: {
    screens: {
      'sm': '640px',
      'md': '768px',
      'xl': '1280px',
      'lg': '1195px',
      'sm_menu': {'max': '767px'},
      'max520': {'max': '520px'},
      'lg-max': {'max': '1194px'},
    },
    listStyleType: {
      none: 'none',
      disc: 'disc',
      decimal: 'decimal',
      square: 'square'
    },
    fontFamily: {
      'body' : ['Liberation Sans Narrow', 'sans-serif'],
      'title': ['Cuprum', 'sans-serif'],
      'solid': ['Oswald', 'sans-serif']
    },
  	extend: {
      flex: {
        '700': '0 1 700px',
        '324': '0 1 324px',
        '800': '0 1 800px',
        '1195': '0 1 1195px',
      },
  		textColor: {
        primary: "var(--color-text-primary)",
        secondary: "var(--color-text-secondary)",
        default: "var(--color-text-default)",
        "default-soft": "var(--color-text-default-soft)",
        inverse: "var(--color-text-inverse)",
        "inverse-soft": "var(--color-text-inverse-soft)",
        link: "var(--color-text-link)",
        hover: "var(--color-text-hover)"
	    },
	    backgroundColor: {
        // primary: "var(--color-bg-primary)",
        primary: "#003e6b",
	      secondary: "var(--color-bg-secondary)",
	      default: "var(--color-bg-default)",
	      inverse: "var(--color-bg-inverse)"
      },
      width: {
        'smevna': "1195px"
      },
      minWidth: {
        'smevna': "1195px"
      },
    },
  },
  variants: {
  },
  plugins: []
}
