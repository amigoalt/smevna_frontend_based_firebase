Понятие "обычай делового оборота" применяется в судебной практике в тех случаях, когда обязательства хозяйствующих субъектов не определены правовой нормой.

.. image:: img/articles/traditions_business.jpg
   :alt: Обычай делового оборота
   :class: mt-4 mb-4

Определение обычая по ГК РФ
===========================

Обычай делового оборота - есть сложившееся и широко применяемое в какой-либо области предпринимательской или иной деятельности, не предусмотренное законодательством правило поведения, независимо от того, зафиксировано ли оно в каком-либо документе (п. 1 ст.5 ГК РФ)

Наряду с термином "обычай" используются и другие близкие по значению термины, такие как: "обыкновения", "обычно предъявляемые требования", "практика, установившаяся во взаимных отношениях сторон" и др. (см. статьи 165.1, 309.2, 314-316 ГК РФ)

Признаки обычаев делового оборота
=================================

Принято выделять следующие признаки правового обычая:

1. Сложившееся правило поведения
2. Обязательное правило поведения
3. Многократное и единообразное правило поведения
4. Правило образовалось в определённой сфере деятельности
5. Правило не конкретизировано в законодательстве, но подпадает под судебную защиту

Особенности применения правого обычая
=====================================

1. Обязательное соответствие обычая положениям законодательства (п. 2 ст. 5 ГК РФ).
2. Обычай является нормой права, а договор определяет соответствующие права и обязанности хозяйствующих сторон ст. 153 ГК РФ

Какие статьи ГК РФ ссылаются на обычаи
======================================

* Статья 5 ГК РФ содержит определение понятия "обычай"; статьи 19, 165.1, 221.
* К понятию "обычай делового оборота" ссылаются следующие статьи раздела "Обязательное право": 309, 311, 312, 314, 316, 368, 406.
* В разделе "Обязательное право" на обычай ссылаются статьи 309, 311, 312, 314, 316, 368, 406.
* В разделе "Договорное право" - статьи 421, 427, 429, 431, 434, 452.
