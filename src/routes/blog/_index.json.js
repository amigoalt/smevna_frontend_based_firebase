import * as api from '../../utils/api.js';

let articles;
let articlesCount;
let contents;

getData('articles');


async function getData(query, user='mmm', token='ttoken') {
	articles = null;

	// TODO do we need some error handling here?
	// ({ articles, articlesCount } = await api.get(query, user && token));
	articles = await api.get(query, user && token);

}

export function get(req, res) {
	res.writeHead(200, {
		'Content-Type': 'application/json'
	});

	const contents = JSON.stringify(articles['articles'].map(post => {
		return {
			title: post.title,
			slug: post.slug,
			id: post.id,
			html_short: post.html.substr(0, 50),
			content_short: post.content.substr(0, 250)
		};
	}));


	res.end(contents);
}