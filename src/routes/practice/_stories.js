// Ordinarily, you'd generate this data from markdown files in your
// repo, or fetch them from a database of some kind. But in order to
// avoid unnecessary dependencies in the starter template, and in the
// service of obviousness, we're just going to leave it here.

// This file is called `_posts.js` rather than `posts.js`, because
// we don't want to create an `/blog/posts` route — the leading
// underscore tells Sapper not to do that.

const posts = [
	{
		title: 'What is Sapper?',
		slug: 'what-is-sapper',
        html: `**Дело № 5-198/2013**


        ##ПОСТАНОВЛЕНИЕ
        
        по делу об административном правонарушении
        г. Йошкар-Ола 29 мая 2014 года
        
        Судья Йошкар-Олинского городского суда Республики Марий Эл Майорова С.М., с участием лица, привлекаемого к административной ответственности Садыкова Ф.Г., защитника – адвоката Смирновой Е.В., представившей удостоверение № и ордер №, потерпевшей З.Т.В., рассмотрев материалы дела об дминистративном правонарушении в отношении
        Садыкова Ф.Г., <данные изъяты>, 
        1. привлекавшегося к административной ответственности по ст. 12.6 КоАП РФ к штрафу в размере 500 рублей,
        2. привлекаемого к административной ответственности по ч. 2 ст. 12.24 Кодекса Российской Федерации об административных правонарушениях,
        
        
        ###УСТАНОВИЛ:
        
        
        ДД.ММ.ГГГГ около 17 часов 25 минут Садыков Ф.Г., управляя автомашиной <данные изъяты>, г/н №, по адресу: <адрес>, в нарушение п. 14.1 Правил дорожного движения РФ при приближении к нерегулируемому пешеходному переходу, обозначенному дорожными знаками 5.19.1 и 5.19.2 «Пешеходный переход» Правил дорожного движения РФ, не уступил
        дорогу пешеходу З.Т.В., переходившей проезжую часть дороги по нерегулируемому пешеходному переходу, и совершил на нее наезд. 
        
        В результате дорожно-транспортного
        происшествия З.Т.В. был причинен вред здоровью средней тяжести.
        
        В судебном заседании Садыков Ф.Г. вину в совершении правонарушения признал, в содеянном раскаялся, просил не лишать его права управления транспортными средствами,
        так как его работа связана с ежедневной необходимостью управления транспортным средством для доставки на строительные объекты рабочих и инструментов. В случае
        лишения его водительских прав он не сможет осуществлять надлежащим образом свои должностные обязанности и может быть уволен с работы. Садыков Ф.Г. пояснил, что ДД.ММ.ГГГГ около 17 часов 25 минут он возвращался с работы, управляя автомашиной <данные изъяты> г/н № двигался по <адрес>. 
        
        Подъезжая к <адрес>, он остановился, пропуская автомашины, двигавшиеся по <адрес>, намереваясь повернуть налево в сторону <адрес>. Перед ним стояла автомашина <данные изъяты>, которая двигалась в попутном с ним направлении. Когда автомашины проехали, он начал движение, выехал на <адрес> со скоростью не более 15 км/ч, посмотрел направо, нет ли там автомашин. Повернувшись вперед, увидел перед своей автомашиной пешехода - женщину, которая двигалась по
        пешеходному переходу от магазина <данные изъяты> в сторону школы №. Он сразу нажал на педаль тормоза, однако дорожно-транспортного происшествия избежать не удалось.
        Полагает, что совершил наезд на потерпевшую из-за того, что она попала в «мертвую зону», кроме того в тот момент шел мокрый снег. Он сообщил о происшествии в ГИБДД, вызвал
        скорую медицинскую помощь. 
        
        Впоследствии он принес З.Т.В. извинения, предлагал компенсировать моральный вред, оплатить лечение. ДД.ММ.ГГГГ он выплатил потерпевшей З.Т.В. в счет компенсации морального вреда, причиненного в результате ДТП, денежные средства в сумме 50000 рублей.
        
        Защитник Садыкова Ф.Г. - адвокат Смирнова Е.В. пояснила, что Садыков Ф.Г. вину в совершении правонарушения признает, возместил потерпевшей ущерб, просила не лишать его права управления транспортными средствами, поскольку данное обстоятельство может негативно сказаться на материальном положении семьи ее доверителя, в связи с возможной потерей места работы.
        
        Потерпевшая З.Т.В. в судебном заседании пояснила, что ДД.ММ.ГГГГ около 17 часов 25 минут она хотела перейти дорогу по нерегулируемому пешеходному переходу вблизи <адрес>, от магазина <данные изъяты> в сторону <данные изъяты>. Перед ней проехала автомашина – черный внедорожник, за ним выехала автомашина <данные изъяты> и встал перед пешеходным переходом. Убедившись, что автомашина пропускает пешеходов, она продолжила движение в сторону <данные изъяты>. В этот момент она почувствовала удар в левый бок, от которого упала на дорогу, ударившись левой стороной лица. Затем поняла, что на нее совершил наезд автомобиль <данные изъяты>. Водитель автомашины помог ей встать, вызвал скорую, попросил прощения и предложил оказать материальную помощь. 
        
        В настоящее время она простила Садыкова Ф.Г., приняла от него в счет возмещения морального вреда денежные средства, претензий материального и морального характера не
        имеет.
        
        Представитель административного органа, составившего протокол об административном правонарушении, - Батальона ДПС ГИБДД УМВД России по <адрес> в судебное заседание не явился, извещен о его месте и времени надлежащим образом.
        Выслушав лицо, в отношении которого ведется производство по делу об административном правонарушении, его защитника, потерпевшую, изучив материалы административного дела, прихожу к следующему.
        
        Частью 2 ст. 12.24 Кодекса Российской Федерации об административных правонарушениях (далее - КоАП РФ) предусмотрена административная ответственность за
        нарушение Правил дорожного движения или правил эксплуатации транспортного средства, повлекшее причинение средней тяжести вреда здоровью потерпевшего.
        
        В соответствии с пунктом 14.1 Правил дорожного движения РФ водитель транспортного средства, приближающегося к нерегулируемому пешеходному переходу,
        обязан снизить скорость или остановиться перед переходом, чтобы пропустить пешеходов, переходящих проезжую часть или вступивших на нее для осуществления перехода.
        
        В судебном заседании Садыков Ф.Г. не отрицал факт того, что им был нарушен п. 14.1 Правил дорожного движения РФ, что привело к дорожно-транспортному происшествию –
        наезду на пешехода З.Т.В. и причинению последней вреда здоровью средней тяжести.
        
        Вина Садыкова Ф.Г. подтверждается исследованными по делу доказательствами:
        * пояснениями потерпевшей З.Т.В., объяснениями Садыкова Ф.Г. от ДД.ММ.ГГГГ, протоколом № об административном правонарушении от ДД.ММ.ГГГГ, пояснениями потерпевшей З.Т.В.,
        * данными в судебном заседании и изложенными выше, справкой по дорожно-транспортному происшествию от ДД.ММ.ГГГГ, схемой места дорожно-транспортного происшествия от ДД.ММ.ГГГГ, протоколом осмотра места совершения административного правонарушения №, предусмотренном ч. 2 ст. 12.24 КоАП РФ, вынесенном в отношении Садыкова Ф.Г.,
        * протоколом осмотра транспортного средства <данные изъяты> г/н № от ДД.ММ.ГГГГ, актом судебно-медицинского освидетельствования № от ДД.ММ.ГГГГ потерпевшей З.Т.В.
        
        
        При осмотре автомашины <данные изъяты> г/н №, проведенном ДД.ММ.ГГГГ, обнаружены повреждения внешнего переднего левого крыла, дефлектора капота.
        Согласно акту судебно-медицинского освидетельствования № от ДД.ММ.ГГГГ у З.Т.В., ДД.ММ.ГГГГ года рождения, обнаружены следующие повреждения: закрытый перелом в
        области ногтевой фаланги 1 пальца правой стопы, ссадины на коже лица. Данные повреждения могли возникнуть от прямого и касательного травматического воздействия тупых твердых предметов, возможно в срок, указанный в направлении, то есть ДД.ММ.ГГГГ.
        
        Данные повреждения повлекли за собой длительное расстройство здоровья продолжительностью свыше трех недель (более 21 дня), и по этому критерию относятся к повреждения, причинившим средней тяжести вред здоровью.
        Таким образом, оценив по правилам ст. 26.11 КоАП РФ и с учетом ч. 3 ст. 26.2 КоАП РФ все имеющиеся доказательства в их совокупности, прихожу к выводу о наличии в действиях
        Садыкова Ф.Г. состава административного правонарушения, предусмотренного ч. 2 ст. 12.24 КоАП РФ.
        
        Решая вопрос о виде и размере административного наказания, исхожу из того, что КоАП РФ допускает возможность назначения административного наказания лишь в пределах
        санкций, установленных законом, предусматривающим ответственность за данное административное правонарушение с учетом характера совершенного правонарушения,
        личности виновного, имущественного положения  равонарушителя, обстоятельств, смягчающих и отягчающих административную ответственность.
        
        Санкция ч. 2 ст. 12.24 КоАП РФ предусматривает наказание в виде административного штрафа в размере от десяти тысяч до двадцати пяти тысяч рублей или лишение права управления транспортными средствами на срок от полутора до двух лет.
        Смягчающими административную ответственность обстоятельствами суд признает раскаяние Садыкова Ф.Г. в содеянном, добровольное возмещение потерпевшей морального
        вреда, наличие на иждивении двоих малолетних детей,  положительную характеристику с места работы, наличие многочисленных грамот за успехи в профессиональной деятельности и спортивных мероприятиях.
        
        Обстоятельством, отягчающим административную ответственность, является совершение Садыковым Ф.Г. повторного однородного административного правонарушения,
        поскольку ранее Садыков Ф.Г., как указано в установочной части постановления, привлекался к административной ответственности к наказанию в виде штрафа, который согласно представленным данным оплачен.
        
        При назначении наказания суд учитывает характер и обстоятельства совершенного Садыковым Ф.Г. правонарушения, данные о его личности, имущественное положение,
        смягчающие и отягчающее административную ответственность обстоятельства, а также мнение потерпевшей З.Т.В., не имеющей претензий к водителю Садыкову Ф.Г. и полагавшей
        возможным не назначать ему наиболее строгий вид наказания, предусмотренный санкцией ч. 2 ст. 12.24 КоАП РФ.
        Исходя из вышеизложенного, считаю возможным назначить Садыкову Ф.Г. административное наказание в виде административного штрафа.
        
        Руководствуясь ст.ст. 29.9, 29.10 Кодекса Российской Федерации об административных правонарушениях, судья
        
        **ПОСТАНОВИЛ:**
        
        Признать Садыкова Ф.Г. виновным в совершении административного правонарушения, предусмотренного частью 2 статьи 12.24 Кодекса Российской Федерации об административных правонарушениях, и назначить административное наказание в виде административного штрафа в размере 10000 (десять тысяч) рублей.
        
        Реквизиты для оплаты административного штрафа:
        <данные изъяты> <данные изъяты>
        
        Постановление может быть обжаловано в Верховный Суд Республики Марий Эл через Йошкар-Олинский городской суд Республики Марий Эл либо непосредственно в Верховный
        
        Суд Республики Марий Эл в течение 10 суток со дня вручения или получения копии
        постановления.
        
        Судья С.М. Майорова`
	},

	{
		title: 'How to use Sapper',
		slug: 'how-to-use-sapper',
		html: `
			<h2>Step one</h2>
			<p>Create a new project, using <a href='https://github.com/Rich-Harris/degit'>degit</a>:</p>

			<pre><code>npx degit "sveltejs/sapper-template#rollup" my-app
			cd my-app
			npm install # or yarn!
			npm run dev
			</code></pre>

			<h2>Step two</h2>
			<p>Go to <a href='http://localhost:3000'>localhost:3000</a>. Open <code>my-app</code> in your editor. Edit the files in the <code>src/routes</code> directory or add new ones.</p>

			<h2>Step three</h2>
			<p>...</p>

			<h2>Step four</h2>
			<p>Resist overdone joke formats.</p>
		`
	},

	{
		title: 'Why the name?',
		slug: 'why-the-name',
		html: `
			<p>In war, the soldiers who build bridges, repair roads, clear minefields and conduct demolitions — all under combat conditions — are known as <em>sappers</em>.</p>

			<p>For web developers, the stakes are generally lower than those for combat engineers. But we face our own hostile environment: underpowered devices, poor network connections, and the complexity inherent in front-end engineering. Sapper, which is short for <strong>S</strong>velte <strong>app</strong> mak<strong>er</strong>, is your courageous and dutiful ally.</p>
		`
	},

	{
		title: 'How is Sapper different from Next.js?',
		slug: 'how-is-sapper-different-from-next',
		html: `
			<p><a href='https://github.com/zeit/next.js'>Next.js</a> is a React framework from <a href='https://zeit.co'>Zeit</a>, and is the inspiration for Sapper. There are a few notable differences, however:</p>

			<ul>
				<li>It's powered by <a href='https://svelte.dev'>Svelte</a> instead of React, so it's faster and your apps are smaller</li>
				<li>Instead of route masking, we encode route parameters in filenames. For example, the page you're looking at right now is <code>src/routes/blog/[slug].html</code></li>
				<li>As well as pages (Svelte components, which render on server or client), you can create <em>server routes</em> in your <code>routes</code> directory. These are just <code>.js</code> files that export functions corresponding to HTTP methods, and receive Express <code>request</code> and <code>response</code> objects as arguments. This makes it very easy to, for example, add a JSON API such as the one <a href='blog/how-is-sapper-different-from-next.json'>powering this very page</a></li>
				<li>Links are just <code>&lt;a&gt;</code> elements, rather than framework-specific <code>&lt;Link&gt;</code> components. That means, for example, that <a href='blog/how-can-i-get-involved'>this link right here</a>, despite being inside a blob of HTML, works with the router as you'd expect.</li>
			</ul>
		`
	},

	{
		title: 'How can I get involved?',
		slug: 'how-can-i-get-involved',
		html: `
			<p>We're so glad you asked! Come on over to the <a href='https://github.com/sveltejs/svelte'>Svelte</a> and <a href='https://github.com/sveltejs/sapper'>Sapper</a> repos, and join us in the <a href='https://svelte.dev/chat'>Discord chatroom</a>. Everyone is welcome, especially you!</p>
		`
	}
];

posts.forEach(post => {
	post.html = post.html.replace(/^\t{3}/gm, '');
});

export default posts;
