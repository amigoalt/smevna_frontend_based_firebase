import * as api from '../../utils/api.js'

const { id } = req.params;
let story = getStory('articles/$id')

async function getStory(query, user='mmm', token='ttoken') {
	story = null;
	// TODO do we need some error handling here?
	// ({ articles, articlesCount } = await api.get(query, user && token));
	story = await api.get(query, user && token);

}


export function get(req, res, next) {
	// the `id` parameter is available because
	// this file is called [id].json.js
	const { id } = req.params;

	if (lookup.has(slug)) {
		res.writeHead(200, {
			'Content-Type': 'application/json'
		});

		res.end(lookup.get(slug));
	} else {
		res.writeHead(404, {
			'Content-Type': 'application/json'
		});

		res.end(JSON.stringify({
			message: `Not found`
		}));
	}
}
