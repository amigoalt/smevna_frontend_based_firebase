import { writable } from 'svelte/store';

/* For admin */
export const editMode = writable(false);

/* === SIDEBAR === */
export const sidebar = writable({
    visible: false,
    items: [
        {'path': '.', 'title': 'Главная', 'slide': 'img/slides/shahmat.jpg'},
        {'path': 'about', 'title': 'Об адвокате', 'slide': 'img/slides/na_share.jpg'},
        {'path': 'prais-list', 'title': 'Прайс-лист', 'slide': 'img/slides/key.jpg'},
        {'path': 'practice', 'title': 'Практика', 'slide': 'img/slides/practice_slide.jpg'},
        {'path': 'blog', 'title': 'Блог', 'slide': 'img/slides/chasi.jpg'},
        {'path': 'contacts', 'title': 'Контакты', 'slide': 'img/slides/lestnitsa.jpg'}
        // {'path': 'admin', 'title': 'admin', 'slide': 'img/slide_admin.jpg'}
    ]
})

let base_api = process.env.NODE_ENV == 'development' ? 'http://localhost:9005' : 'http://advokatsmirnova.ru:85';
let base_site = process.env.NODE_ENV == 'development' ? 'http://localhost:3000' : 'http://advokatsmirnova.ru';
export const base = writable({
    api: base_api,
    site: base_site
})


export const slug = 'blog';